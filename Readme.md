## Prerequisite

- PHP >= 7.1.3
- mariadb = 10.3.29
- [Symfony server last version](https://symfony.com/doc/current/setup/symfony_server.html)

## Installation

1 :In your terminal, in the folder you want to install this project launch the following command to download the project from gitlab.

```bash
git clone git@gitlab.com:MathieuDelrot/todo-and-co.git
```

2 :In your terminal go to the project folder and launch the following command to launch Symfony server

```bash
symfony server:start
```

3 :Start your mysql server with MAMP, WAMP ou Mysql

4 :Install dependencies
```bash
composer install
```

5 :Create database with following command

```bash
php bin/console doctrine:database:create
```

6 :Create schema with following command

```bash
php bin/console doctrine:schema:create
```

## Fixtures and tests

1 :Launch datas fixtures

```bash
php bin/console doctrine:fixtures:load
```

2 :Launch tests

```bash
bin/phpunit
```

3 :Test code coverage

```bash
bin/phpunit --coverage-html ./tests-coverage
```

## Install PHP CS and PHP MD to check code quality
1 : Create folder to install packages
```bash
mkdir tools/php-cs-fixer
mkdir tools/phpmd
```
2 : Install packages
```bash
composer require --working-dir=tools/php-cs-fixer squizlabs/php_codesniffer
composer require --working-dir=tools/phpmd phpmd/phpmd
```
3 : Launch following commands to test your code
```bash
tools/php-cs-fixer/vendor/bin/phpcb
tools/phpmd/vendor/bin/phpmd 
```
4 : Apply corrections

## Contribution

To contribute to this project, create an issue, a merge request associated, do tests valid your code with PHP CS and PHP MD and push your code. Merge request will be approuve by the team.
