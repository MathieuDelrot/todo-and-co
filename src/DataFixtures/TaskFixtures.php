<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $task = new Task();
        $task->setContent("Installer le projet")
            ->setTitle("Télécharger la dernière version stable de Symfony avec le web skeleton et lancer le server")
            ->setUser($manager->getRepository(User::class)->findOneBy(['username' => 'user1']));
        $manager->persist($task);

        $task = new Task();
        $task->setContent("Création de l'entité atelier")
            ->setTitle("Avec la commande make:entity créer l'entité Workshop selon le modèle défini")
            ->setUser($manager->getRepository(User::class)->findOneBy(['username' => 'user2']));
        $manager->persist($task);

        $task = new Task();
        $task->setContent("Création du controller pour les ateliers")
            ->setTitle("Avec la commande make:crud créer le controller et les templates permettant d'ajouter, de modifier et de supprimer un atelier")
            ->setUser($manager->getRepository(User::class)->findOneBy(['username' => 'useradmin']))
            ->setIsDone(true);
        $manager->persist($task);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return array(
            UserFixtures::class,
        );
    }
}
