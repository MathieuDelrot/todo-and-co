<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail("user$i@email.fr")
                ->setPassword('Puser' . $i)
                ->setUsername("user$i");

            $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

            $manager->persist($user);
        }

        $user = new User();
        $user->setEmail("useradmin@email.fr")
            ->setPassword('Puseradmin1')
            ->setUsername("useradmin")
            ->setRoles(['ROLE_ADMIN']);

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

        $manager->persist($user);

        $anonymousUser = new User();
        $anonymousUser->setUsername('Anonyme');
        $anonymousUser->setPassword('Anonyme');
        $anonymousUser->setEmail('anonyme@yopmail.com');
        $anonymousUser->setRoles(["ROLE_USER"]);
        $anonymousUser->setPassword($this->passwordEncoder->encodePassword($anonymousUser, $anonymousUser->getPassword()));

        $manager->persist($anonymousUser);

        $manager->flush();
    }
}
