<?php

namespace App\Command;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class SetDefaultUserToOldTaskCommand
 * @package App\Command
 * @codeCoverageIgnore
 */
class SetDefaultUserToOldTaskCommand extends Command
{
    protected static $defaultName = 'app:setDefaultUserToOldTask';
    protected static $defaultDescription = 'Add a short description for your command';
    private $entityManager;

    /**
     * SetDefaultUserToOldTaskCommand constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }


    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->addDefaultUserToNotAssignedTask();

        $io->success('Command done');

        return 0;
    }
    private function addDefaultUserToNotAssignedTask()
    {

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $anonymousUser = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'Anonyme']);
        if (!$anonymousUser) {
            $anonymousUser = $this->createAnonymousUser();
        }

        foreach ($tasks as $task) {
            if (!$task->getUser() || $task->getUser()->getId() === 0) {
                $task->setUser($anonymousUser);
                $this->entityManager->flush();
            }
        }
    }

    private function createAnonymousUser(): User
    {
        $anonymousUser = new User();
        $anonymousUser->setUsername('Anonyme');
        $anonymousUser->setPassword('Anonyme');
        $anonymousUser->setEmail('anonyme@yopmail.com');
        $anonymousUser->setRoles(["ROLE_USER"]);

        $this->entityManager->persist($anonymousUser);
        $this->entityManager->flush();

        return $anonymousUser;
    }
}
