<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/task")
 */
class TaskController extends AbstractController
{
    /**
     * @Route("/", name="task_index", methods={"GET"})
     * @param TaskRepository $taskRepository
     * @return Response
     */
    public function index(TaskRepository $taskRepository): Response
    {
        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findAllOrderByIsDone(),
        ]);
    }

    /**
     * @Route("/todo", name="task_list_to_do", methods={"GET"})
     * @param TaskRepository $taskRepository
     * @return Response
     */
    public function toDo(TaskRepository $taskRepository): Response
    {
        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findBy([
                'isDone' => 0
            ]),
        ]);
    }

    /**
     * @Route("/done", name="task_list_done", methods={"GET"})
     * @param TaskRepository $taskRepository
     * @param TaskRepository $taskRepository
     * @return Response
     */
    public function Done(TaskRepository $taskRepository): Response
    {
        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findBy([
                'isDone' => 1
            ]),
        ]);
    }



    /**
     * @Route("/new", name="task_new", methods={"GET","POST"})
     * @param Request $request
     * @param Security $security
     * @return Response
     */
    public function new(Request $request, Security $security): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $task->setUser($security->getUser());
            $entityManager->persist($task);
            $entityManager->flush();

            $this->addFlash('success', sprintf('La tâche %s a bien été ajoutée', $task->getTitle()));

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/new.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="task_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Task $task): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', sprintf('La tâche %s a bien été modifiée', $task->getTitle()));

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/done/{id}", name="task_done")
     */
    public function doneTaskAction(Task $task): Response
    {
        if ($task->isDone() === false) {
            $message = 'La tâche %s a bien été marquée comme faite.';
        } else {
            $message = 'La tâche %s est ré-ouverte';
        }
        $task->setIsDone(!$task->isDone());
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', sprintf($message, $task->getTitle()));

        return $this->redirectToRoute('task_index');
    }

    /**
     * @Route("/delete/{id}", name="task_delete", methods={"POST"})
     * @param Request $request
     * @param Task $task
     * @param Security $security
     * @return Response
     */
    public function delete(Request $request, Task $task, Security $security): Response
    {
        $delete = false;
        if (!$this->isCsrfTokenValid('delete' . $task->getId(), $request->request->get('_token')) || !$security->getUser()) {
            $request->getSession()->getFlashBag()->add('error', 'Token de formulaire invalide OU vous n\'êtes pas connecté');
        }
        if ($task->getUser()->getUsername() === 'Anonyme') {
            if (!$this->isGranted('ROLE_ADMIN')) {
                $request->getSession()->getFlashBag()->add('error', 'Vous devez être connecté en tant qu\'admin pour supprimer cette tâche');
            } else {
                $this->removeTask($request, $task);
                $delete = true;
            }
        }
        if ($delete === false && $security->getUser() === $task->getUser()) {
            $this->removeTask($request, $task);
            $delete = true;
        } elseif ($delete === false) {
            $request->getSession()->getFlashBag()->add('error', 'Vous pouvez supprimer seulement les tâches que vous avez créés');
        }

        return $this->redirectToRoute('task_index');
    }

    private function removeTask(Request $request, Task $task)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($task);
        $entityManager->flush();
        $request->getSession()->getFlashBag()->add('success', 'La tâche a été supprimée');
    }
}
