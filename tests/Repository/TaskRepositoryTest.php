<?php


namespace App\Tests\Repository;


use App\DataFixtures\TaskFixtures;
use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskRepositoryTest extends KernelTestCase
{
    use FixturesTrait;

    public function setUp() : void
    {
        self::$kernel = self::bootKernel();
    }

    public function testCount()
    {
        $this->loadFixtures([TaskFixtures::class]);

        $nbTasks = self::$container->get(TaskRepository::class)->count([]);
        $this->assertEquals(3, $nbTasks);
    }

    public function testInvalid()
    {
        $task = new Task();
        $task->setContent('')
            ->setCreatedAt(new \DateTimeImmutable('now'))
            ->setTitle('');

        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->expectException('Doctrine\DBAL\Exception\NotNullConstraintViolationException');

        $entityManager->persist($task);
        $entityManager->flush();
    }

}