<?php


namespace App\Tests\Repository;


use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserRepositoryTest extends KernelTestCase
{
    use FixturesTrait;

    private $passwordEncoder;

    public function setUp(): void
    {
        self::bootKernel();

        $this->passwordEncoder = self::$container->get('Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface');

    }

    public function testCount()
    {

        $this->loadFixtures([UserFixtures::class]);

        $nbUsers = self::$container->get(UserRepository::class)->count([]);
        $this->assertEquals(11, $nbUsers);
    }

    public function testNonUniqueEmail()
    {
        $user = new User();

        $user->setEmail("user30@email.fr")
            ->setPassword('thisIsTHePassword2')
            ->setUsername("user30");

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $entityManager->persist($user);
        $entityManager->flush();

        $user = new User();

        $user->setEmail("user30@email.fr")
            ->setPassword('thisIsTHePassword2')
            ->setUsername("user31");

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

        $this->expectException('Doctrine\DBAL\Exception\UniqueConstraintViolationException');

        $entityManager->persist($user);
        $entityManager->flush();
    }

    public function testNonUniqueUserName()
    {
        $user = new User();

        $user->setEmail("user25@email.fr")
            ->setPassword('thisIsTHePassword2')
            ->setUsername("user25");

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $entityManager->persist($user);
        $entityManager->flush();
        $user = new User();

        $user->setEmail("user26@email.fr")
            ->setPassword('thisIsTHePassword2')
            ->setUsername("user25");

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

        $this->expectException('Doctrine\DBAL\Exception\UniqueConstraintViolationException');

        $entityManager->persist($user);
        $entityManager->flush();
    }

}