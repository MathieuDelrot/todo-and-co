<?php


namespace App\Tests\Controller\UserController;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserHomeControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([UserFixtures::class]);
    }

    public function testPageDisplayed()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $client->request('GET', '/user/');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $this->assertSelectorTextContains('h1', 'Liste des utilisateurs');
    }

    public function testPageIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/user/');
        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');
    }

    public function testDisplayedAddButtonIfAdminConnected()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $client->request('GET', '/user/');

        $this->assertSelectorTextNotContains('a', 'Ajouter un utilisateur');

    }

    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}