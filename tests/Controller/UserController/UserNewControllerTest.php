<?php


namespace App\Tests\Controller\UserController;


use App\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserNewControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([UserFixtures::class]);
    }

    public function testAddPageIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/user/new');
        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');
    }

    public function testAddWithSuccess()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $crawler = $client->request('GET', '/user/new');

        $form = $crawler->selectButton('Ajouter un utilisateur')->form([
            'user[username]' => 'UserNameTest',
            'user[email]' => 'UserNameTest@test.fr',
            'user[password]' => 'PassWrids1'
        ]);

        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');
    }

    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }
}
