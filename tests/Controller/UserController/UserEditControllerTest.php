<?php


namespace App\Tests\Controller\UserController;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Task;
use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserEditControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([UserFixtures::class]);
    }

    public function testPageIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();

        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['username' => 'user0']);

        $id = $user->getId();

        $client->request('GET', '/user/edit/' . $id);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testEditWithSuccess()
    {
        $client = self::clientWithConnectedAdmin();

        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['username' => 'user0']);

        $id = $user->getId();

        $crawler = $client->request('GET', '/user/edit/' . $id);

        $form = $crawler->selectButton('Modifier')->form([
            'user[username]' => 'UserNameTest',
            'user[email]' => 'UserNameTest@test.fr',
            'user[password]' => 'PassWrids1'
        ]);


        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');
    }


    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}