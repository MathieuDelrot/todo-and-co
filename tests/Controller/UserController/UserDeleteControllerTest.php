<?php


namespace App\Tests\Controller\UserController;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Task;
use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserDeleteControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([UserFixtures::class]);
    }

    public function testDeleteWithSuccess()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['username' => 'user0']);

        $id = $user->getId();

        $crawler = $client->request('GET', '/user/edit/' . $id);

        $form = $crawler->selectButton('Supprimer')->form();

        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');
    }


    public function testDeleteWithCsrfError()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['username' => 'user0']);


        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/user/delete/'.$user->getId());

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');
    }


    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}