<?php


namespace App\Tests\Controller;


use App\DataFixtures\TaskFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskToDoControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([TaskFixtures::class]);
    }

    public function testPageDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/todo');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testPageContentDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/todo');

        $this->assertSelectorTextContains('h1', 'Liste des tâches');
    }

    public function testTaskDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/todo');

        $this->assertSelectorExists('div:contains("Télécharger la dernière version stable de Symfony avec le web skeleton et lancer le server")');

    }

    public function testTaskNotDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/todo');

        $this->assertSelectorNotExists('div:contains("Avec la commande make:crud créer le controller et les templates permettant d\'ajouter, de modifier et de supprimer un atelier")');

    }

}