<?php


namespace App\Tests\Controller;


use App\DataFixtures\TaskFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskNewControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([TaskFixtures::class]);
    }

    public function testPageIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/new');
        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testAddWithSuccess()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $crawler = $client->request('GET', '/task/new');

        $form = $crawler->selectButton('Ajouter')->form([
            'task[title]' => 'This is a task',
            'task[content]' => 'a test task'
        ]);

        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');

        $this->assertSelectorTextContains('h4', 'This is a task');
    }

    public function testAddIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();

        $client->request('GET', '/task/new');

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }



    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}