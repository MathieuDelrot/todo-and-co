<?php


namespace App\Tests\Controller\TaskController;


use App\DataFixtures\TaskFixtures;
use App\Entity\Task;
use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskDeleteControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([TaskFixtures::class]);
    }

    public function testIsDeleteButtonIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();

        $client->request('GET', '/task/');

        $this->assertSelectorNotExists('button', 'Supprimer');

    }

    public function testIsDeleteButtonIfConnected()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $client->request('GET', '/task/');

        $this->assertSelectorExists('button', 'Supprimer');

    }


    public function testDeleteWithErrorNotGoodUserToDelete()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $crawler = $client->request('GET', '/task/');

        $form = $crawler->selectButton('Supprimer')->form();

        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');
    }

    public function testDeleteWithSuccess()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneByUsername('useradmin');

        $task = $client->getContainer()->get('doctrine')->getRepository(Task::class)->findOneByUser($user);


        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/task/delete/'.$task->getId(), [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');
    }

    public function testDeleteWithCsrfError()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $user = $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneByUsername('useradmin');

        $task = $client->getContainer()->get('doctrine')->getRepository(Task::class)->findOneByUser($user);


        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/task/delete/'.$task->getId());

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');
    }


    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}