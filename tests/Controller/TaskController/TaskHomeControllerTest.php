<?php


namespace App\Tests\Controller;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskHomeControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([TaskFixtures::class]);
    }

    public function testPageDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testPageContentDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/');

        $this->assertSelectorTextContains('h1', 'Liste des tâches');
    }

    public function testAlertText()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/');

        $this->assertSelectorTextContains('span', 'Vous devez être connecté pour ajouter une tâche.');
    }

    public function testTaskDisplayed()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/task/');

        $this->assertSelectorExists('div:contains("Télécharger la dernière version stable de Symfony avec le web skeleton et lancer le server")');

    }

    public function testNeedConnectedTextDisplayed()
    {
        self::ensureKernelShutdown();

        $client = self::clientWithConnectedAdmin();

        $client->request('GET', '/task/');

        $this->assertSelectorTextNotContains('span', 'Vous devez être connecté pour ajouter une tâche.');


    }

    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}