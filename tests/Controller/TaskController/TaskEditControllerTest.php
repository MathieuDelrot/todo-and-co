<?php


namespace App\Tests\Controller;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TaskEditControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([TaskFixtures::class]);
    }

    public function testNotDisplayedPageIfNotConnected()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();

        $task = $client->getContainer()->get('doctrine')->getRepository(Task::class)->findOneBy(['title' => 'Avec la commande make:crud créer le controller et les templates permettant d\'ajouter, de modifier et de supprimer un atelier']);

        $id = $task->getId();

        $client->request('GET', '/task/edit/'.$id);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-danger');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testEditWithSuccess()
    {
        $client = self::clientWithConnectedAdmin();

        $task = $client->getContainer()->get('doctrine')->getRepository(Task::class)->findOneBy(['title' => 'Avec la commande make:crud créer le controller et les templates permettant d\'ajouter, de modifier et de supprimer un atelier']);

        $id = $task->getId();

        $crawler = $client->request('GET', '/task/edit/'.$id);

        $form = $crawler->selectButton('Mettre à jour')->form([
            'task[title]' => 'This is a test task',
            'task[content]' => 'a test task'
        ]);

        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');
    }


    private static function clientWithConnectedAdmin(): \Symfony\Bundle\FrameworkBundle\KernelBrowser
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        return $client;
    }

}