<?php


namespace App\Tests\Controller;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    use FixturesTrait;

    public function setUp(): void
    {
        $this->loadFixtures([UserFixtures::class]);
    }

    public function testRedirectAfterLogin()
    {
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/login');

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $client->request('POST', '/login', [
            '_csrf_token' => $csrfToken,
            'username' => 'useradmin',
            'password' => 'Puseradmin1'
        ]);

        $client->followRedirect();

        $this->assertSelectorExists('.alert.alert-success');

        $this->assertSelectorTextContains('h1', 'Bienvenue sur Todo List, l\'application vous permettant de gérer l\'ensemble de vos tâches sans effort !');

    }
}