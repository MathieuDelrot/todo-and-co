<?php


namespace App\Tests\Entity;


use App\DataFixtures\UserFixtures;
use App\Entity\Task;
use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskEntityTest extends KernelTestCase
{
    use FixturesTrait;

    public function setUp() : void
    {
        self::bootKernel();

        $this->loadFixtures([UserFixtures::class]);
    }

    public function testValidEntity()
    {

        $task = new Task();
        $task->setContent("Ceci est une tâche de test")
            ->setTitle("Création d'une tâche test permettant de tester l'ajout des tâches")
            ->setUser(self::$container->get(UserRepository::class)->findOneBy(['username' => 'user1']));

       $error = self::$container->get('validator')->validate($task);

       $this->assertCount(0, $error);
    }

    public function testInvalidEntity()
    {
        $task = new Task();

        //On s'attend à 2 erreurs car le title et le content sont vides.

        $this->assertCount(2, self::$container->get('validator')->validate($task));
    }

    public function testInvalidIsDone()
    {
        $task = new Task();
        $task->setContent("Ceci est une tâche de test")
            ->setTitle("Création d'une tâche test permettant de tester l'ajout des tâches")
            ->setUser(self::$container->get(UserRepository::class)->findOneBy(['username' => 'user1']))
            ->setIsDone('hh');

        $this->assertCount(1, self::$container->get('validator')->validate($task));
    }

}