<?php


namespace App\Tests\Entity;


use App\DataFixtures\TaskFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Task;
use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserEntityTest extends KernelTestCase
{
    use FixturesTrait;

    private $passwordEncoder;

    public function setUp(): void
    {
        self::bootKernel();

        $this->passwordEncoder = self::$container->get('Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface');
        $this->loadFixtures([UserFixtures::class]);
        $this->loadFixtures([TaskFixtures::class]);

    }


    public function testValidEntity()
    {
        $user = new User();

        $user->setEmail("user22@email.fr")
            ->setPassword('thisIsTHePassword2')
            ->setUsername("user22");

        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));

        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(0, $error);
    }

    public function testEmptyPassword()
    {
        $user = new User();

        $user->setEmail("user23@email.fr")
            ->setUsername("user23");

        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(1, $error);
    }

    public function testInvalidPassword()
    {
        $user = new User();

        $user->setEmail("user24@email.fr")
            ->setPassword('Ete2')
            ->setUsername("user24");

        $user->setUsername($this->passwordEncoder->encodePassword($user, $user->getPassword()));


        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(1, $error);
    }

    public function testEmptyUserName()
    {
        $user = new User();

        $user->setPassword('Eteej2')
            ->setEmail('md@fr');

        $user->setUsername($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(1, $error);
    }

    public function testInvalidEmail()
    {
        $user = new User();

        $user->setPassword('Eteej2')
            ->setEmail('m@fr')
            ->setUsername("user27");

        $user->setUsername($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(1, $error);

        $user = new User();

        $user->setPassword('Etee2l')
            ->setEmail('@fr.er')
            ->setUsername("user28");

        $user->setUsername($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(1, $error);
    }

    public function testEmptyEmail()
    {
        $user = new User();

        $user->setPassword('Eteej2')
            ->setUsername("user29");

        $user->setUsername($this->passwordEncoder->encodePassword($user, $user->getPassword()));
        $error = self::$container->get('validator')->validate($user);

        $this->assertCount(1, $error);
    }

    public function testGetTask()
    {
        $user = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()->getRepository(User::class)->findOneBy(['username' => 'user2']);
        $nbTasks = $user->getTasks()->count();

        $this->assertEquals(1, $nbTasks);
    }

    public function testAddTask()
    {
        $task = new Task();
        $task->setTitle('Test add by user entity')
            ->setContent('This is content');

        $entityManager = self::$kernel->getContainer()
                ->get('doctrine')
                ->getManager();

        /** @var User $user */
        $user = $entityManager->getRepository(User::class)->findOneBy(['username' => 'user2']);
        $user->addTask($task);

        $entityManager->persist($task);
        $entityManager->flush();

        $nbTasks = $user->getTasks()->count();

        $this->assertEquals(2, $nbTasks);
    }

    public function testRemoveTask()
    {

        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        /** @var User $user */
        $user = $entityManager->getRepository(User::class)->findOneBy(['username' => 'user2']);

        $task = $entityManager->getRepository(Task::class)->findOneBy(['user' => $user]);

        $user->removeTask($task);

        $nbTasks = $user->getTasks()->count();

        $this->assertEquals(0, $nbTasks);
    }

}